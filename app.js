// const express = require("express");
// const app = express();
// const cors = require("cors")

// const cookieParser = require("cookie-parser");

// //directory match...
// const path = require("path");

// app.use(express.json({ limit: "25mb" }));
// app.use(express.urlencoded({ limit: "25mb", extended: true }));

// //serves static page
// app.use(express.static(path.join(__dirname, "/view")));

// //database
// require("./database/mongoose");

// app.use("/uploads", express.static(path.join(__dirname, "/uploads")));
// //routes
// const userRouter = require("./route/userRoutes");
// const adminRouter = require("./route/adminRoute");

// // app.use((req, res, next) => {
// //     const allowedOrigins = ["https://homesweathome-git-master-karma-wangchuk.vercel.app"];
// //     const origin = req.headers.origin;
// //     if (allowedOrigins.includes(origin)) {
// //       res.setHeader("Access-Control-Allow-Origin", origin);
// //     }
// //     res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
// //     res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
// //     next();
// //   });

// app.use(cors({
//     origin: "https://homesweathome-git-master-karma-wangchuk.vercel.app",
//     methods: "GET, POST, PUT, DELETE",
//     allowedHeaders: "Content-Type, Authorization",
//   }));

// app.use(cookieParser());
// app.use(userRouter);
// app.use(adminRouter);
// module.exports = app;

const express = require("express");
const app = express();
const cors = require("cors");

const cookieParser = require("cookie-parser");

//directory match...
const path = require("path");

app.use(express.json({ limit: "25mb" }));
app.use(express.urlencoded({ limit: "25mb", extended: true }));

//serves static page
app.use(express.static(path.join(__dirname, "/view")));

//database
require("./database/mongoose");

app.use("/uploads", express.static(path.join(__dirname, "/uploads")));

// Enable CORS
app.use(cors({
  origin: "https://homesweathome-git-master-karma-wangchuk.vercel.app",
  methods: "GET, POST, PUT, DELETE",
  allowedHeaders: "Content-Type, Authorization",
}));

app.use(cookieParser());

//routes
const userRouter = require("./route/userRoutes");
const adminRouter = require("./route/adminRoute");

app.use(userRouter);
app.use(adminRouter);

module.exports = app;
