const express = require("express");
const controller = require("../controller/userController");
// const { userAuth } = require("../middleware/auth");

//for emailVerifications
const emailVerifications = require("../controller/emailController");

const router = express.Router();
console.log(__dirname);
router.post("/user/signup", controller.registerUser);
router.post("/user/login", controller.loginUser);
router.get("/user/logout", controller.logoutUser);

// module.exports = router;

// const express = require("express");
// const controller = require("../controller/userController");
// // const { userAuth } = require("../middleware/auth");

// // for emailVerifications
// const emailVerifications = require("../controller/emailController");
// const router = express.Router();
// const app = express(); // Create an Express app

// // Middleware for handling CORS
// app.use((req, res, next) => {
//   res.setHeader("Access-Control-Allow-Origin", "https://homesweathome-acfd3uwvi-karma-wangchuk.vercel.app");
//   res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
//   res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
//   next();
// });

// Routes
// app.post("https://dbandserver.onrender.com//user/signup", controller.registerUser);
// app.post("https://dbandserver.onrender.com//user/login", controller.loginUser);
// app.get("/user/logout", controller.logoutUser);

// app.listen(3000, () => {
//   console.log("Server listening on port 3000");
// });

module.exports = router;