const express = require("express");
const multer = require("multer");
const controller1 = require("../controller/adminController");
const controller2 = require("../controller/blogController");
const controller3 = require("../controller/videoController");
// const { adminAuth } = require("../middleware/auth");
const router = express.Router();

//serve static files

//create multer storage
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "uploads/");
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "-" + file.originalname);
  },
});

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 25, // 25MB limit (adjust as needed)
  },
  dest: "../uploads/",
});

router.post("/admin/signup", controller1.signupAdmin);
router.post("/admin/login", controller1.loginAdmin);
router.get("/admin/logout", controller1.logoutAdmin);

router.post(
  "/admin/blog_post",
  upload.single("blog_image"),
  controller2.createBlogPost
);

//get single blog
router.get("/admin/get_blog/:id", controller2.getBlogPost);
router.delete("/admin/blog_delete/:id", controller2.deleteBlogPost);
router.patch("/admin/blog_update/:id", controller2.updateBlogPost);
router.get("/admin/Get_allBlog", controller2.getAllBlogPosts);

//video routes
router.post("/admin/add_video", controller3.addVideo);
router.put("/admin/update_video/:id", controller3.updateVideo);
router.delete("/admin/delete_video/:id", controller3.deleteVideo);
router.get("/admin/getone_video/:id", controller3.getOneVideo);
router.get("/admin/getAll/:workout_type", controller3.getAllVideo);
router.get("/admin/getAll", controller3.getEveryVideo);

module.exports = router;

// const express = require("express");
// const multer = require("multer");
// const controller1 = require("../controller/adminController");
// const controller2 = require("../controller/blogController");
// const controller3 = require("../controller/videoController");
// const router = express.Router();
// const app = express(); // Create an Express app

// // Middleware for handling CORS
// app.use((req, res, next) => {
//   res.setHeader("Access-Control-Allow-Origin", "https://homesweathome-acfd3uwvi-karma-wangchuk.vercel.app");
//   res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
//   res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
//   next();
// });

// // serve static files

// // create multer storage
// const storage = multer.diskStorage({
//   destination: (req, file, cb) => {
//     cb(null, "uploads/");
//   },
//   filename: (req, file, cb) => {
//     cb(null, Date.now() + "-" + file.originalname);
//   },
// });

// const upload = multer({
//   storage: storage,
//   limits: {
//     fileSize: 1024 * 1024 * 25, // 25MB limit (adjust as needed)
//   },
//   dest: "../uploads/",
// });

// // Routes
// app.post("/admin/signup", controller1.signupAdmin);
// app.post("/admin/login", controller1.loginAdmin);
// app.get("/admin/logout", controller1.logoutAdmin);

// app.post(
//   "/admin/blog_post",
//   upload.single("blog_image"),
//   controller2.createBlogPost
// );

// // get single blog
// app.get("/admin/get_blog/:id", controller2.getBlogPost);
// app.delete("/admin/blog_delete/:id", controller2.deleteBlogPost);
// app.patch("/admin/blog_update/:id", controller2.updateBlogPost);
// app.get("/admin/Get_allBlog", controller2.getAllBlogPosts);

// // video routes
// app.post("/admin/add_video", controller3.addVideo);
// app.put("/admin/update_video/:id", controller3.updateVideo);
// app.delete("/admin/delete_video/:id", controller3.deleteVideo);
// app.get("/admin/getone_video/:id", controller3.getOneVideo);
// app.get("/admin/getAll/:workout_type", controller3.getAllVideo);
// app.get("/admin/getAll", controller3.getEveryVideo);

// module.exports = router;